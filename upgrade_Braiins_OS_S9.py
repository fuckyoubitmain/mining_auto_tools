#!/usr/bin/env python

import sys, paramiko
import time
import warnings
from cryptography.utils import DeprecatedIn25
warnings.simplefilter('ignore', DeprecatedIn25)

if len(sys.argv) < 1:
    print("args missing")
    sys.exit(1)

hostname = sys.argv[1]
k = paramiko.RSAKey.from_private_key_file("~/.ssh/antminer")
try:
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, port="22", username="root", pkey=k, timeout=4)
    #client.connect(hostname, port=22, username="root", password="root", timeout=4)
    sftp =  client.open_sftp()
    sftp.put('firmware.tar', '/tmp/firmware.tar')
    stdin, stdout, stderr = client.exec_command("sysupgrade -F /tmp/firmware.tar")
    print(stdout.read())
    time.sleep(60)
    client.close()
    # update Braiins OS+
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, port="22", username="root", pkey=k, timeout=4)
    stdin, stdout, stderr = client.exec_command("opkg update")
    print(stdout.read())
    time.sleep(60)
    client.close()
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, port="22", username="root", pkey=k, timeout=4)
    stdin, stdout, stderr = client.exec_command("opkg install bos_plus")
    print(stdout.read())
    time.sleep(60)
    client.close()
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(hostname, port="22", username="root", pkey=k, timeout=4)
    stdin, stdout, stderr = client.exec_command("/etc/init.d/bosminer stop")
    sftp =  client.open_sftp()
    sftp.put('bosminer-plus-am1-s9_freq-fix_uio-fix', '/usr/bin/bosminer')
    stdin, stdout, stderr = client.exec_command('echo "[autotuning]" >> /etc/bosminer.toml')
    stdin, stdout, stderr = client.exec_command('echo "enabled = true" >> /etc/bosminer.toml')
    stdin, stdout, stderr = client.exec_command('echo "psu_power_limit = 850" >> /etc/bosminer.toml')
    stdin, stdout, stderr = client.exec_command("/etc/init.d/bosminer start")
    print(stdout.read())
finally:
    client.close()
